requirejs.config({
    baseUrl: 'script'
});

requirejs(['game', '1-dawn'], function (game, dawn) {
    var actions = {
        yes: function () {
            dawn.start();
        },
        no: function () {
            console.log("Boo.");
        }
    };

    game.setActions(actions);

    console.log("Would you like to start?");
    console.info("game:", game.game());
});
