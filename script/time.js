define(function () {
    if (!window.time) {
        window.time = new Date(2017, 8, 11, 7, 45, 0);
    }

    var addSeconds = function (seconds) {
        window.time.setSeconds(window.time.getSeconds() + seconds);
    };

    return {
        time: window.time,
        addSeconds: addSeconds
    }
});
