define(['game', 'time'], function (game, time) {
    var alarm,
        start_alarm,
        pause_alarm;

    start_alarm = function () {
        console.log("BEEP BEEP BEEP BEEP!");

        alarm = setInterval(function () {
            console.log("BEEP BEEP BEEP BEEP!");
            time.addSeconds(7);
        }, 5000);
    };

    pause_alarm = function (in_seconds) {
        clearInterval(alarm);

        setTimeout(start_alarm, in_seconds * 1000);
    };

    actions = {
        look: function () {
            var minutes = ('0' + time.time.getMinutes()).substr(-2),
                alarm_time = 'The only light in the room is the alarm clock, flashing ' + time.time.getHours() + ':' + minutes + '.',
                message = alarm_time;

            if (!this.standing) {
                message = 'Your bed is very comfy. ' + message;
            }

            return message;
        },
        snooze_alarm: function () {
            nine_minutes = 9 * 60;
            time.addSeconds(nine_minutes);
            pause_alarm(20);

            return 'Without even opening your eyes, you reflexively hit the snooze button.';
        },
        turn_off_alarm: function () {
            clearInterval(alarm);

            return "Peace returns to your room.";
        },
        stand: function () {
            return "It wasn't easy, but you made it. This is half the battle won!";
        },
    };

    return {
        start: function () {
            game.setActions(actions);
            start_alarm();
        }
    };
});
