define(function () {
    if (!window.game) {
        window.game = {};
    }

    var setGame = function setGame(game) {
        window.game = game;
    }

    return {
        game: function () { return window.game; },
        setActions: setGame
    };
});
